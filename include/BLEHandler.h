#ifndef GADGET_BLEHANDLER_H
#define GADGET_BLEHANDLER_H

int setupBLE ();

bool isConnected();

void disconnect();

void sendDataAsJSON(int color, int vibrationIntensity);

void sendData(String data);

#endif //GADGET_BLEHANDLER_H
