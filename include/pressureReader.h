#ifndef GADGET_PRESSUREREADER_H
#define GADGET_PRESSUREREADER_H

#include <Arduino.h>

void setupPressureReader(int pInterval = 0);

int readPressure(int threshold = 0);

#endif //GADGET_PRESSUREREADER_H

