//
// Created by Oliver on 13.05.2023.
//

#ifndef GADGET_BUTTONREADER_H
#define GADGET_BUTTONREADER_H

void setupButtonReader(int pInterval = 0);

bool buttonIsPressed();

#endif //GADGET_BUTTONREADER_H
