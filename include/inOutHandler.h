#ifndef GADGET_INOUTHANDLER_H
#define GADGET_INOUTHANDLER_H

void setupInOut();

std::tuple<int, int> handleInOut();

void replayMessage(String messageString);

#endif //GADGET_INOUTHANDLER_H
