#ifndef GADGET_RAINBOWLED_H
#define GADGET_RAINBOWLED_H

#include <Arduino.h>

void initRainbowLED(int pInterval = 10);

void showSpectrum();

void showSpectrumOnce();

void showRGB(int colorIndex, int maxBrightness = 255);

void blankLED();

void initBlink();

void blinkLED();

#endif //GADGET_RAINBOWLED_H
