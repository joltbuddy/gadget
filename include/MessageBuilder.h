//
// Created by Regrets on 07/07/2023.
//

#ifndef GADGET_MESSAGEBUILDER_H
#define GADGET_MESSAGEBUILDER_H


#include <string>
#include "Arduino.h"

class MessageBuilder {
private:
    unsigned long lastMillis;
    std::string message;
    int lastColor, lastVib, startTime;

public:
    bool recording = false;

    void appendToMessage(int color, int vib);

    void startMessage(int color, int vib);

    bool isRecording();

    String endMessage();
};


#endif //GADGET_MESSAGEBUILDER_H
