#include <Arduino.h>
#include <tuple>

#include "Globals.h"
#include "rainbowLED.h"
#include "inOutHandler.h"
#include "BLEHandler.h"
#include "Wait.h"
#include "MessageBuilder.h"

Wait SendRateLimiter;
MessageBuilder MessageBuilder;

bool startupDone = false;

bool playingBack = false;

bool sending = false;

void initLED () {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
}

void setup()
{
    initLED();

    // initInOut
    setupInOut();

    // show startupRainbow
    showSpectrumOnce();
    blankLED();

    initBlink();

    setupBLE();

    // init SendRateLimiter
    SendRateLimiter = Wait(33);

    startupDone = true;
}

int colorCode, vibrationIntensity;
unsigned long currentMillis = 0;
unsigned long lastTick = 0;
long timeout = 0;
unsigned long delta;
void loop()
{
    if (!startupDone) return;

    if (!isConnected()) {
        blinkLED();
        return;
    }

    // This needs to be here because in isConnected() we indirectly invoke the BLE handling which is needed for eventing
    if (playingBack || sending) return;

    std::tuple<int, int> inputParameters = handleInOut();
    std::tie(colorCode, vibrationIntensity) = inputParameters;

    // TODO: Make smart. i.e. Build message here by saving all new inputs to a list of some sort
    // If list is not updated for e.g. 2 seconds, compress it so we only have values, start and end time per datapoint
    // Then pass it to a new function that "streams" it to the app in chunks
    currentMillis = millis();
    if (SendRateLimiter.run() && colorCode != -1 && vibrationIntensity != -1) {
        if (!MessageBuilder.isRecording()){ //first input
            MessageBuilder.appendToMessage(colorCode,vibrationIntensity);
            timeout = 2000; // starts timeout
            lastTick = currentMillis;
            return;
        } else{ // input other than -1, but while recording
            timeout = 2000;
        }
    }
    //every loop
    if (MessageBuilder.isRecording()) {
        //calculate time since last loop
        delta = currentMillis - lastTick;
        if (delta < timeout) { //check if there is sufficient time left
            timeout -= delta; //reduce timeout
            MessageBuilder.appendToMessage(colorCode, vibrationIntensity);
        } else {
            sendData(MessageBuilder.endMessage());
        }
        lastTick = currentMillis;
    }
}
