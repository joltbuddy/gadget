#include <Arduino.h>
#include "rainbowLED.h"
#include "Wait.h"

#define RedPin 3
#define GreenPin 4
#define BluePin 5

#define BlinkDelay 1300
#define BlinkDuration 100

int colorIndex = 0;

Wait SpectrumWaitHelper;

Wait BlinkWaitHelper;

void initRainbowLED(int pInterval) {
    // init rgb LED
    pinMode(RedPin, OUTPUT);
    pinMode(GreenPin, OUTPUT);
    pinMode(BluePin, OUTPUT);

    SpectrumWaitHelper = Wait(pInterval);
}

int dimToMaxBrightness(int pInput, int pMaxBrightness = 255) {
    return map(pInput, 0, 255, 0, pMaxBrightness);
}

// showRGB()

// This function translates a number between 0 and 767 into a
// specific color on the RGB LED. If you have this number count
// through the whole range (0 to 767), the LED will smoothly
// change color through the entire spectrum.

// The "base" numbers are:
// 0   = pure red
// 255 = pure green
// 511 = pure blue
// 767 = pure red (again)

// Numbers between the above colors will create blends. For
// example, 640 is midway between 512 (pure blue) and 767
// (pure red). It will give you a 50/50 mix of blue and red,
// resulting in purple.

// If you count up from 0 to 767 and pass that number to this
// function, the LED will smoothly fade between all the colors.
// (Because it starts and ends on pure red, you can start over
// at 0 without any break in the spectrum).

void showRGB(int color, int maxBrightness) {
    int redIntensity;
    int greenIntensity;
    int blueIntensity;

    if (color <= 255)          // zone 1 red -> green
    {
        redIntensity = 255 - color;    // red goes from on to off
        greenIntensity = color;        // green goes from off to on
        blueIntensity = 0;             // blue is always off
    } else if (color <= 511)     // zone 2 green -> blue
    {
        redIntensity = 0;                     // red is always off
        greenIntensity = 255 - (color - 256); // green on to off
        blueIntensity = (color - 256);        // blue off to on
    } else // color >= 512       // zone 3 blue -> red
    {
        redIntensity = (color - 512);         // red off to on
        greenIntensity = 0;                   // green is always off
        blueIntensity = 255 - (color - 512);  // blue on to off
    }

    analogWrite(RedPin, dimToMaxBrightness(redIntensity, maxBrightness));
    analogWrite(BluePin, dimToMaxBrightness(blueIntensity, maxBrightness));
    analogWrite(GreenPin, dimToMaxBrightness(greenIntensity, maxBrightness));
}

// showSpectrum()

// This function steps through all the colors of the RGB LED.
// It does this by stepping a variable from 0 to 768 (the total
// number of colors), and repeatedly calling showRGB() to display
// the individual colors.

void showSpectrum() {
    if (!SpectrumWaitHelper.run()) return;

    showRGB(colorIndex++);

    // If we've reached the end of the spectrum, start over.
    if (colorIndex >= 768) {
        colorIndex = 0;
    }
}

void showSpectrumOnce() {
    const int maxColors = 224;
    for (int i = 0; i < maxColors;) {
        int color = map(i++, 0, maxColors, 0, 767);
        showRGB(color);
        delay(2);
    }
}

void blankLED() {
    analogWrite(RedPin, 0);
    analogWrite(BluePin, 0);
    analogWrite(GreenPin, 0);
}

void initBlink() {
    BlinkWaitHelper = Wait(BlinkDelay);
}

void blinkLED() {
    if (!BlinkWaitHelper.run()) return;

    if (BlinkWaitHelper.interval == BlinkDelay) {
        showRGB(511, 10);
        BlinkWaitHelper = Wait(BlinkDuration);
    } else if (BlinkWaitHelper.interval == BlinkDuration) {
        blankLED();
        BlinkWaitHelper = Wait(BlinkDelay);
    }
}
