#include <Arduino.h>

#include <tuple>

#include "pressureReader.h"
#include "buttonReader.h"
#include "rainbowLED.h"
#include "Globals.h"


#define VibrationPin 6

#define VibrationSensorThreshold 250
#define VibrationSensorEstimatedMax 800
#define VibrationMin 100
#define VibrationMax 255

bool vibrationMode = false;
int vibrationModeThresholdMinPressure = 300;


void setupInOut() {
    // init button
    setupButtonReader();

    // init rgb LED
    initRainbowLED(10);

    // init pressure sensor
    setupPressureReader(0);

    // init vibration motor
    pinMode(VibrationPin, OUTPUT);
}

void resetOut() {
    blankLED();
    vibrationMode = false;
    analogWrite(VibrationPin, LOW);
}

std::tuple<int, int> getInputParameters() {
    int pressureReading = readPressure(VibrationSensorThreshold);
    int colorCode = min(map(pressureReading, 0, VibrationSensorEstimatedMax, 100, 767), 767);

    // If no pressure, vibrate stronger. Only if more pressure than a defined threshold is applied, offer the whole range
    int vibrationIntensity = 200;
    if (vibrationMode || pressureReading > vibrationModeThresholdMinPressure) {
        vibrationMode = true;
        vibrationIntensity = min(map(pressureReading, 0, VibrationSensorEstimatedMax, VibrationMin, VibrationMax), 255);
    }

    return {colorCode, vibrationIntensity};
}

void writeToOutput(int color, int vibrationIntensity) {
    analogWrite(VibrationPin, vibrationIntensity);
    showRGB(color);
}

std::tuple<int, int> handleInput () {
    int colorCode, vibrationIntensity;
    std::tuple<int, int> inputParameters = getInputParameters();

    std::tie(colorCode, vibrationIntensity) = inputParameters;

    writeToOutput(colorCode, vibrationIntensity);

    return inputParameters;
}

std::tuple<int, int> handleInOut() {
    if (buttonIsPressed()) {
        int colorCode, vibrationIntensity;
        std::tuple<int, int> inputParameters = handleInput();

        std::tie(colorCode, vibrationIntensity) = inputParameters;

        return inputParameters;
    } else {
        resetOut();
        return {-1, -1};
    }
}

std::array<int, 4> parseTick(const String& tick) {
    int valueSeparator = tick.indexOf(',');
    int dataSeparator = tick.indexOf(',', valueSeparator + 1);
    int metaSeparator = tick.indexOf(',', dataSeparator + 1);


    int color = tick.substring(0, valueSeparator).toInt();
    int vibration = tick.substring(valueSeparator + 1, dataSeparator).toInt();
    int startTime = tick.substring(dataSeparator + 1, metaSeparator).toInt();
    int endTime = tick.substring(metaSeparator + 1).toInt();

    return {color, vibration, startTime, endTime};
}

void replayMessage(String messageString) {
    playingBack = true;

    bool finished = false;

    Serial.println(messageString);

    int ms = 0;

    while (!finished) {
        int tickSeparatorIndex = messageString.indexOf(";");
        String tick = messageString.substring(0, tickSeparatorIndex);

        std::array<int, 4> parsedTick = parseTick(tick);

        int color = parsedTick[0];
        int vibrationIntensity = parsedTick[1];
        int startTime = parsedTick[2];
        int endTime = parsedTick[3];

        while(ms <= endTime) {
            if (ms < startTime) {
                resetOut();
            } else {
                if (color == -1 && vibrationIntensity == -1) {
                    resetOut();
                } else {
                    writeToOutput(color, vibrationIntensity);
                }
            }

            ms++;

            // We need about 450 microseconds for the loop, comparisons and writing to out. Waiting 550 microseconds
            // then results in each tick being roughly 1ms in duration wich results in accurate enough playback.
            delayMicroseconds(550);
        }


        messageString = messageString.substring(tickSeparatorIndex + 1);

        if (messageString == "") {
            finished = true;
        }
    }

    playingBack = false;
}
