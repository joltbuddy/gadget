#include <Arduino.h>
#include "Wait.h"

#define PressurePin A0

Wait pressureWaitHelper;

void setupPressureReader(int pReadInterval = 0) {
    pinMode(PressurePin, INPUT);
    pressureWaitHelper = Wait(pReadInterval);
}

int readPressure(int threshold = 0) {
    if (!pressureWaitHelper.run()) return 0;

    int pressureReading = analogRead(PressurePin);

    if (pressureReading < threshold) return 0;

    return pressureReading;
}
