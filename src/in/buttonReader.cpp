#include <Arduino.h>
#include "Wait.h"

#define ButtonPin 2

Wait buttonWaitHelper;

void setupButtonReader(int pReadInterval = 0) {
    pinMode(ButtonPin, INPUT);
    buttonWaitHelper = Wait(pReadInterval);
}

bool buttonIsPressed() {
    if (!buttonWaitHelper.run()) return false;

    if (digitalRead(ButtonPin) == HIGH) return true;

    return false;
}
