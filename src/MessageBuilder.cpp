//
// Created by Regrets on 07/07/2023.
//

#include "MessageBuilder.h"
#include <Arduino.h>
#include <iostream>
#include <string>

bool MessageBuilder::isRecording(){
    return recording;
}

void MessageBuilder::startMessage(int color, int vib) {
    recording = true;
    startTime = 0;
    lastMillis = millis();
    message = std::to_string(color) + ',' + std::to_string(vib) + ',' + std::to_string(startTime);
    lastColor = color;
    lastVib = vib;
    Serial.println("Start Message");
}

void MessageBuilder::appendToMessage(int color, int vib){
    if (!recording){
        startMessage(color,vib);
        return;
    }
    //skip same packets
    short diff = vib-lastVib;
    if (!(diff > 10 || diff < -10)){
        return;
    }
    unsigned int delta = millis()-lastMillis;
    unsigned int endTime = startTime+delta;
    startTime = endTime+1;
    message+= ','+std::to_string(endTime)+";"+std::to_string(color) + ',' + std::to_string(vib) + ',' + std::to_string(startTime);
    lastMillis = millis();
    lastColor = color;
    lastVib = vib;
    Serial.println("Appended"+String(endTime)+","+String(color)+","+String(startTime));
}

String MessageBuilder::endMessage(){
    unsigned long endTime = startTime+(millis()-lastMillis);
    message+=','+std::to_string(endTime)+";";
    recording = false;
    Serial.println(message.c_str());
    return {message.c_str()};
}

