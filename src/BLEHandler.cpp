#include <ArduinoBLE.h>

#include <utility>
#include "inOutHandler.h"

#include "Globals.h"

int messageSize = 244; // This seems to be the max an arduino nano 33 ble supports
unsigned int bufferSize = messageSize;
String rawData;
unsigned int totalRawDataLength = 0;
bool hasOverflow = false;
unsigned int currentDataIndex = 0;
unsigned int maximumDataIndex = 0;

String receivedMessage;

BLEService dataService("181C");
BLEStringCharacteristic dataCharacteristic("2A31", BLERead | BLENotify , messageSize);
BLEStringCharacteristic notificationCharacteristic("2A46", BLEWrite, messageSize);

void BLEConnectedHandler(BLEDevice central) {
    Serial.println("Connected event, central: ");
    Serial.println(central.address());
    BLE.stopAdvertise();
}

void BLEDisconnectedHandler(BLEDevice central) {
    Serial.println("Disconnected event, central: ");
    Serial.println(central.address());
    BLE.advertise();
}

void BLENotificationCharacteristicUpdatedHandler(BLEDevice central, BLEStringCharacteristic characteristic) {
    Serial.println("Updated characteristic");

    // This should contain a string with values, including start and end times
    // e.g. <color>,<vibration>,<startMS>,<endMS>;100,200,60,134
    String value = characteristic.value();

    if (value.equals("END")) {
        replayMessage(receivedMessage);
        receivedMessage = "";
    } else {
        Serial.println(value);
        receivedMessage.concat(value);
    }
}


String getPacketByIndex (size_t index) {
    if (index == 0 || index < totalRawDataLength / bufferSize) {
        return rawData.substring(index * bufferSize, index * bufferSize + bufferSize);
    } else {
        unsigned int overflowBufferSize = totalRawDataLength - totalRawDataLength % bufferSize;

        return rawData.substring((totalRawDataLength - totalRawDataLength % overflowBufferSize)) + "_";
    }
}

void sendPacketByIndex(size_t index) {
    String packet;

    if (index == 0 || index < maximumDataIndex) {
        packet = getPacketByIndex(index);
    } else {
        packet = "_END";
        currentDataIndex = 0;
        maximumDataIndex = 0;
        sending = false;
    }

    // IMPORTANT: The last packet send, regardless if it is just "END" or the additional "overflow" packet of a String
    // that couldn't be evenly divided may contain stray characters from the last packet. This is a limitation of the
    // BLE String characteristic and requires checking for "END" by checking if the value includes it and removing
    // everything after the first "_" to only get valid message data.
    dataCharacteristic.writeValue(packet);
}

void BLEDataReadHandler() {
    Serial.println("Data Characteristic read");

    if (maximumDataIndex > 0) {
        currentDataIndex++;
        sendPacketByIndex(currentDataIndex);
    }
}

int setupBLE () {
    if (!BLE.begin()) {   // initialize BLE
        Serial.println("starting BLE failed!");
        return -1;
    }

    BLE.setEventHandler(BLEConnected, BLEConnectedHandler);
    BLE.setEventHandler(BLEDisconnected, BLEDisconnectedHandler);

    BLE.setLocalName("JoltBuddyKeychain");

    dataCharacteristic.writeValue("Connected, no Data yet.");
    dataCharacteristic.setEventHandler(BLERead, reinterpret_cast<BLECharacteristicEventHandler>(BLEDataReadHandler));

    notificationCharacteristic.setEventHandler(BLEWritten,
                                               reinterpret_cast<BLECharacteristicEventHandler>(BLENotificationCharacteristicUpdatedHandler));

    BLE.setAdvertisedService(dataService);
    dataService.addCharacteristic(dataCharacteristic);
    dataService.addCharacteristic(notificationCharacteristic);
    BLE.addService(dataService);

    BLE.advertise();
    return 0;
}

bool isConnected () {
    BLEDevice central = BLE.central();
    if (central.connected()) {
        return true;
    }

    return false;
}

void disconnect () {
    BLE.disconnect();
}

void sendData(String data) {
    sending = true;

    rawData = std::move(data);
    totalRawDataLength = rawData.length();

    hasOverflow = totalRawDataLength % bufferSize;

    maximumDataIndex = totalRawDataLength / bufferSize;
    if (hasOverflow) {
        maximumDataIndex++;
    }

    currentDataIndex = 0;

    sendPacketByIndex(currentDataIndex);
}

void sendDataAsJSON(int color, int vibrationIntensity) {
    if (!isConnected()) return;

    dataCharacteristic.writeValue(String(R"({"color":)" + String(color) + R"(,"vibrationIntensity":)" + String(vibrationIntensity) + R"(})"));
}
